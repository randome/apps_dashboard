Info
====

Check out <http://shopify.github.com/dashing> for more information.

Installation
===
There is a windows service running called AscioDashboard which was installed with
<https://github.com/gsmedley/thin_service>

If you need to reinstall the service for whatever reason, make sure there are the following folders created :

- app
- config
- log

Adding Widgets
===
You can find different widgets under `/widgets` folder.

Jobs are located under `/jobs` folder

Just follow the examples or look at `localhost:3030/sample`


Contributing
===
1. `git clone this_project`
2. `bundle`
3. `dashing start`


Requirements
===
Go to <http://railsinstaller.org/> and get Windows Installer for `Ruby 1.9`