require "httpclient"
require "nokogiri"


def fetch_running_workflows
  url = "http://ldn-vm-grpgw01.auth.netbenefit.com:23000/"
  client = HTTPClient.new
  html = client.get_content(url)
  html_doc = Nokogiri::HTML(html)
  data = html_doc.search('//td/text()').last.to_s
  data[/Running: (\d+)/, 1]
end

task_services = {
  'TaskService01' => 'http://ldn-vm-grpwfe01.auth.netbenefit.com:23000/',
  'TaskService02' => 'http://ldn-vm-grpwfe02.auth.netbenefit.com:23000/',
  'TaskService03' => 'http://ldn-vm-grpwfe03.auth.netbenefit.com:23000/'
}
def fetch_task_service_number(url)
  client = HTTPClient.new
  html = client.get_content(url)
  data = html_doc = Nokogiri::HTML(html).to_s
  data[/Requests: (\d+)/, 1]
end

def fetch_nic_gateway_status
  url = "http://ldn-vm-grpgw01.auth.netbenefit.com:9998/"
  client = HTTPClient.new
  html = client.get_content(url)
  html_doc = Nokogiri::HTML(html)
  html_doc.search('tr[bgcolor]')
end

rows = fetch_nic_gateway_status
rows.each do |row|
  status = row.children.to_a[1].content
  if status != "OK"
    puts row.children.to_a
  end
end

#puts rows.first.children.to_a[1].content