require "httpclient"
require "nokogiri"

task_services = {
  'TaskService01' => 'http://ldn-vm-grpwfe01.auth.netbenefit.com:23000/',
  'TaskService02' => 'http://ldn-vm-grpwfe02.auth.netbenefit.com:23000/',
  'TaskService03' => 'http://ldn-vm-grpwfe03.auth.netbenefit.com:23000/'
}

def fetch_task_service_requests(url)
  client = HTTPClient.new
  html = client.get_content(url)
  data = html_doc = Nokogiri::HTML(html).to_s
  data[/Requests: (\d+)/, 1]
end


SCHEDULER.every '2s' do
  data = Hash.new({ value: 0 })

  task_services.each do |name, url|
    requests_data = fetch_task_service_requests(url)
    data[name] = {
      label: name,
      value: requests_data
    }
  end

  send_event('tasks', { items: data.values })
end