require "httpclient"
require "nokogiri"


def fetch_nic_gateway_status
  url = "http://ldn-vm-grpgw01.auth.netbenefit.com:9998/"
  client = HTTPClient.new
  html = client.get_content(url)
  html_doc = Nokogiri::HTML(html)
  html_doc.search('tr[bgcolor]')
end

SCHEDULER.every '2s' do
  data = Hash.new({ value: 0 })
  title = "All OK"
  moreinfo = "NIC Connections"

  rows = fetch_nic_gateway_status

  rows.each do |row|
    status = row.children.to_a[1].content
    name = row.children.to_a[0].content
    if status != "OK"
      data[name] = {
        label: name,
        value: status
      }
    end
  end

  if !data.empty?
    title = ""
  end

  send_event('nicconnections', {
    items: data.values,
    title: title,
    moreinfo: moreinfo })
end