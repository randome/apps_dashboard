require "httpclient"
require "nokogiri"

points = []
(1..10).each do |i|
  points << { x: i, y: 0 }
end
last_x = points.last[:x]

def fetch_wfe_data
  url = "http://ldn-vm-grpgw01.auth.netbenefit.com:23000/"
  client = HTTPClient.new
  html = client.get_content(url)
  html_doc = Nokogiri::HTML(html)
end

def get_running_workflows(html_doc)
  data = html_doc.search('//td/text()').last.to_s
  data[/Running: (\d+)/, 1]
end

def get_retry_workflows(html_doc)
  data = html_doc.search('//td/text()').last.to_s
  data[/Retry: (\d+)/, 1]
end

def get_pending_workflows(html_doc)
  data = html_doc.search('//td/text()').last.to_s
  data[/Pending: (\d+)/, 1]
end

SCHEDULER.every '2s' do
  html_doc = fetch_wfe_data
  running_workflows = get_running_workflows html_doc
  retry_workflows = get_retry_workflows html_doc
  pending_workflows = get_pending_workflows html_doc

  points.shift
  last_x += 1
  points << { x: last_x, y: running_workflows }

  send_event('wfe_running', {points: points})
  send_event('wfe_retry', {value: retry_workflows})
  send_event('wfe_pending', {value: pending_workflows})
end